Vue.component('greeting', {
    template: '<a href=""><b>Eventually I hope me to be the best</b></a>',
    data: function () {
        return {
            name: 'Yoshiba'
        }
    }
});

var vue = new Vue({
    el: '#app',
    data: {
        message: 'Hello Vue!',
        link: 'https://google.com',
        name: '',
        ageof: '',
        age: 25,
        x: 0,
        y: 0,
        hi: 'hello',
        see: false,
        available: true,
        color: '',
        margin: '',
        success: false,
        error: false,
        characters: ['Maria', 'Luigi', 'Zopinho'],
        ninjas: [
            {name: 'Maria', age: 25},
            {name: 'Luigi', age: 22},
            {name: 'Zopinho', age: 28},
        ]
    },
    methods: {
         greet: function (time) {
            return 'Good' + ' ' + time + ' ' + this.message;
        },
         add: function (inc) {
          this.age += inc;
         },
         sub: function (dec) {
           this.age -= dec;
        },
        updateXY: function (event) {
            this.x = event.offsetX;
            this.y = event.offsetY;
        },
        clickHref: function(){
             alert('You clicked it')
        },
        seeFunc: function () {
            this.see = !this.see;
        },
        logName: function () {
            console.log('you entered your name')
        },
        logAge: function () {
            console.log('you entered your age')
        },
    }
});

var vue_app = new Vue({
    el: '#vue-app',
    data: {

    },
    methods: {

    }
});